## Interface: 60000
## DefaultState: Enabled
## Title: |cFF007FFFO3|r Bigwigs Gateway
## Author: Oz
## Version: 0.1
## Notes: O3 Bigwigs Gateway
## SavedVariablesPerCharacter: O3BWGSettings

O3.lua
Module.lua
Modules\CombatLog.lua
Modules\BWGateway.lua
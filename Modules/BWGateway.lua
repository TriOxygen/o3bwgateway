(function(addon, ns)
	ns.Engine:setEnvironment()

	local _PREFIX = 'O3BW'

	local gateway = Engine:module({
		config = {
			enabled = true,
			font = O3.Media:font('Normal'),
			fontSize = 10,
			fontStyle = '',
		},
		allowedFunctions = {
			createBar = true,
		},
		acronyms = {
			rb = 'createRemoteBar',
		},
		settings = {},
		events = {
			CHAT_MSG_ADDON = true,
		},
		name = 'BigWigs',
		readable = 'BigWigs gateway',
		send = function (self, message, channel, target)
			SendAddonMessage(_PREFIX, message, channel, target)
		end,
		whisper = function (self, message, target)
			SendAddonMessage(_PREFIX, message, 'WHISPER', target)
		end,
		sendToGuild = function (self, message, channel)
			SendAddonMessage(_PREFIX, message, 'GUILD')
		end,
		sendToRaid = function (self, message, channel)
			SendAddonMessage(_PREFIX, message, 'RAID')
		end,
		createRemoteBar = function (self, time, text, player)
			if (time) then
				time = tonumber(time)
			end
			SendAddonMessage(_PREFIX, 'createBar:'..time..':'..text, 'WHISPER', player)
		end,
		hasPrivilege = function (self, sender)
			for raidIndex = 1, MAX_RAID_MEMBERS do
				local name, rank, subgroup, level, class, fileName, zone, online, isDead, role, isML = GetRaidRosterInfo(raidIndex)
				if name and name == sender and rank > 0 then
					return true
				end
			end
			return false
		end,
		CHAT_MSG_ADDON = function (self, prefix, message, channel, sender)
			if prefix ~= _PREFIX then
				return
			end
			local sender = sender:gsub("%-.+", "")
			local func, arg1, arg2 = message:match("^(.*):(.*):(.*)$")
			if func and arg1 and arg2 and self:hasPrivilege(sender) then
				self[func](self, arg1, arg2, sender)
			end
		end,
		createBar = function (self, time, text, from)
			local time = tonumber(time)
			if SlashCmdList.BIGWIGSLOCALBAR then
				SlashCmdList.BIGWIGSLOCALBAR(time..' '..text)
			else
				Engine:error('Bigwigs is not loaded. Type /bw once to load id.')
			end
		end,
		postInit = function (self)
			local success = RegisterAddonMessagePrefix(_PREFIX)

			_G['SLASH_O3BW1'] = "/o3bw"
			SlashCmdList.O3BW = function (msg, editBox)
				local func, arg1, arg2, arg3, arg4 = msg:match("^(%S*)%s*(%S*)%s*(%S*)%s*(%S*)%s*(%S*)$")
				func = self.acronyms[func] or func
				if (func and self[func]) then
					self[func](self, arg1, arg2, arg3, arg4)
					return
				end
				local func, arg1, arg2, arg3 = msg:match("^(%S*)%s*(%S*)%s*(%S*)%s*(%S*)$")
				func = self.acronyms[func] or func
				if (func and self[func]) then
					self[func](self, arg1, arg2, arg3)
					return
				end
				local func, arg1, arg2 = msg:match("^(%S*)%s*(%S*)%s*(%S*)$")
				func = self.acronyms[func] or func
				if (func and self[func]) then
					self[func](self, arg1, arg2)
					return
				end
				local func, arg1 = msg:match("^(%S*)%s*(%S*)$")
				func = self.acronyms[func] or func
				if (func and self[func]) then
					self[func](self, arg1)
					return
				end
			end
		end,		
	})

	_G['O3BWGateway'] = gateway

end)(...)
(function(addon, ns)
	ns.Engine:setEnvironment()

	local Module = Engine.Class:extend({
		name = nil,
		Engine = nil,
		weight = 1,
		config = {
			enabled = true,
		},
		settings = {
		},
		setup = function (self)
			self.frame = CreateFrame('Frame', addon..'Frame', UIParent)
			self:setupEventHandler()
			self:postCreate()
		end,
		postCreate = function (self)
		end,
		enable = function (self)
			if (not self.frame) then
				self:setup()
			end
			self.settings.enabled = true
			if (self.panel) then
				self.panel:show()
			elseif (self.frame) then
				self.frame:Show()
			end
			if (self._eventHandlerInitialized) then
				self:reRegisterEvents()
				self:reset()
			else
				self._eventHandlerInitialized = true
				self:registerEvents()
			end
		end,
		reset = function (self)
		end,
		disable = function (self)
			self.settings.enabled = false
			self:unregisterEvents()
			if (self.panel) then
				self.panel:hide()
			elseif (self.frame) then
				self.frame:Hide()
			end
		end,
		resetSettings = function (self)
			self.settings = {}
			setmetatable(self.settings, {__index = self.config})
		end,
		enabledSet = function (self)
			if (self.settings.enabled) then
				self:enable()
			else
				self:disable()
			end
		end,
		init = function (self)
			self:preInit()	
			self:postInit()
		end,
		VARIABLES_LOADED = function (self)
			
		end,
		register = function (self, Engine)
		end,	
		preInit = function (self)
		end,
		postInit = function (self)
		end,
	})

	Engine.Module = Module

	local EmptyModule = Module:extend({
		empty = function (self, ...)
		end,
	})

	setmetatable(EmptyModule, {
		__index = function (self, key)
			return self.empty
		end
	})

	Engine.module = function (self, template)
		if not template.name then
			template.name = 'EngineModule'..self.counter
			self.counter = self.counter+1
		end
		template.Engine = self
		template.options = {}
		Engine.EventHandler:mixin(template)
		local mod = Module:instance(template)
		self:register(mod)

		return mod

	end
end)(...)